# Fazendo o primeiro push #

Instruções usadas para fazer o primeiro push. 

### Iniciando o projeto ###
```
#!git

git init
```
### Incluindo um arquivo no git ###
```
#!git

git add arquivo.txt
```

### Dando o primeiro commit ###
```
#!git

git commit -m "Meu primeiro commit"
```

### Configurando o repositório remoto ###
```
#!git

git remote add origin https://r2luna@bitbucket.org/r2luna/cursogit.git
```

### Dando um push completo para o repositório remoto ###
```
#!git

git push -u origin --all
```


